import Layout from '@/views/layout/Layout'
import models from '@/modules/models/routes'
import historyReads from '@/modules/history-reads/routes'
import paths from '@/modules/paths/routes'
import points from '@/modules/points/routes'
import robots from '@/modules/robots/routes'
import routes from '@/modules/routes/routes'

export default {
  path: '/robo',
  component: Layout,
  redirect: '/paths/list',
  name: 'Geral Robô',
  meta: { title: 'Robôs', icon: 'reddit' },
  children: [
    ...historyReads,
    ...models,
    ...paths,
    ...points,
    ...robots,
    ...routes
  ]
}
