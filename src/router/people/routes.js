import Layout from '@/views/layout/Layout'
import companies from '@/modules/companies/routes'
import departments from '@/modules/departments/routes'
import people from '@/modules/people/routes'

export default {
  path: '/usuarios',
  component: Layout,
  redirect: '/people/list',
  name: 'Geral Usuários',
  meta: { title: 'Pessoas', icon: 'user' },
  children: [
    ...companies,
    ...departments,
    ...people
  ]
}
