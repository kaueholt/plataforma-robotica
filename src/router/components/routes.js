import Layout from '@/views/layout/Layout'
import devices from '@/modules/devices/routes'
import measures from '@/modules/measures/routes'
import modelDevices from '@/modules/model-devices/routes'
import types from '@/modules/types/routes'

export default {
  path: '/componentes',
  component: Layout,
  redirect: '/device/list',
  name: 'Geral Componentes',
  meta: { title: 'Sensores', icon: 'cpu' },
  children: [
    ...devices,
    ...measures,
    ...modelDevices,
    ...types
  ]
}
