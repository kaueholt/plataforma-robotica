import Vue from 'vue'
import Vuex from 'vuex'
// import routines from '@/modules/routines/store'
import app from './modules/app'
import companies from '@/modules/companies/store'
import departments from '@/modules/departments/store'
import devices from '@/modules/devices/store'
import historyReads from '@/modules/history-reads/store'
import measures from '@/modules/measures/store'
import modelDevices from '@/modules/model-devices/store'
import models from '@/modules/models/store'
import paths from '@/modules/paths/store'
import people from '@/modules/people/store'
import points from '@/modules/points/store'
import robots from '@/modules/robots/store'
import routes from '@/modules/routes/store'
import types from '@/modules/types/store'
import user from './modules/user'

import getters from './getters'
// import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

// const vuexLocal = new VuexPersistence({
//   storage: window.localStorage
// })

const store = new Vuex.Store({
  modules: {
    // routines,
    app,
    companies,
    departments,
    devices,
    historyReads,
    measures,
    modelDevices,
    models,
    paths,
    people,
    points,
    robots,
    routes,
    types,
    user
  },
  getters
  // plugins: [vuexLocal.plugin]
})

export default store
