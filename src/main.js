import Vue from 'vue'
import App from './App.vue'

import './plugins/element.js'
import '@/icons'
// import '@/styles/33robotics-theme/index.scss'
import '@/styles/index.scss'
import 'normalize.css/normalize.css'
// import { faTachometerAlt } from '@fortawesome/free-solid-svg-icons'
// import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
// import { library } from '@fortawesome/fontawesome-svg-core'
import JsonExcel from 'vue-json-excel'
import router from './router'
import store from './store'

// library.add(faTachometerAlt)

Vue.component('downloadExcel', JsonExcel)
// Vue.component('font-awesome-icon', FontAwesomeIcon)

// Vue.use(ElementUI, { locale })

Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
