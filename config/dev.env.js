"use strict";
const merge = require("webpack-merge");
const prodEnv = require("./prod.env");

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_API: '"http://localhost:3000/api/v1"'
  // BASE_API: '"http://ec2-54-94-124-51.sa-east-1.compute.amazonaws.com:8000/api/v1"'
});
